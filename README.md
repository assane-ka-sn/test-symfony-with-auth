TEST SYMFONY FOR BEYE
========================

Développement
Back-end avec Symfony3.4

### Cloner le projet
* Run `git clone https://github.com/assane-ka-sn/test-symfony-for-beye`

# cd test-symfony-for-beye
* composer install
* configure parameters.yml(if not exist copy parameters.yml.dist and rename in parameters.yml) for add the parametres of database
* php bin/console doctrine:database:create 
* php bin/console doctrine:schema:update --force
* php bin/console doctrine:fixtures:load
* php bin/console debug:router // For list route
* php bin/console server:run
