<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Get;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;




/**
 * Category controller.
 *
 * @Route("categories")
 */
class CategoryController extends FOSRestController
{

    /**
     * Lists all category entities.
     *
     * @View()
     * @Get("/")
     */
    public function indexAction()
    {
        $categories = $this->getDoctrine()->getRepository('AppBundle:Category')->findAll();

        return $categories;
    }

    /**
     * Finds and displays a category entity.
     *
     * @View()
     * @Get(path = "/{categoryId}", name = "category_show", requirements = {"categoryId"="\d+"})
     */
    public function showAction(Request $request)
    {
        $category = $this->getDoctrine()->getRepository('AppBundle:Category')->find($request->get('categoryId'));
        if (empty($category)) {
            return new JsonResponse(['message' => 'Category not found'], Response::HTTP_NOT_FOUND);
        }
        return $category;
    }

    /**
     * Creates a new category entity.
     *
     * @View()
     * @Post(path = "", name = "category_new")
     * @ParamConverter("category", converter="fos_rest.request_body")
     */
    public function newAction(Category $category)
    {

        $categoryNew = $this->getDoctrine()->getRepository('AppBundle:Category')->findOneByName($category->getName());

        if (empty($categoryNew)) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            $categoryNew = $category;
        }

        //return $this->redirectToRoute('category_show', array('categoryId' => $categoryNew->getId()));
        return new JsonResponse(['message' => 'Category edited successfully'], Response::HTTP_OK);

    }

    /**
     * Edit an existing category entity
     *
     * @View()
     * @Put(path = "/{categoryId}", name = "category_edit", requirements = {"categoryId"="\d+"})
     */
    public function editAction(Request $request)
    {

        $categoryEdit = $this->getDoctrine()->getRepository('AppBundle:Category')->find($request->get('categoryId'));

        if (empty($categoryEdit)) {
            return new JsonResponse(['message' => 'Category not found for edit'], Response::HTTP_NOT_FOUND);
        }
        $categoryNew = $this->getDoctrine()->getRepository('AppBundle:Category')->findOneByName($request->get('name'));

        if (empty($categoryNew)) {
            $categoryEdit->setName($request->get('name'));
            $em = $this->getDoctrine()->getManager();
            $em->merge($categoryEdit);
            $em->flush();

            return $this->redirectToRoute('category_show', array('categoryId' => $categoryEdit->getId()));
        }

        return new JsonResponse(['message' => 'Category already exist'], Response::HTTP_NOT_FOUND);
    }

    /**
     * Deletes a category entity.
     *
     * @View()
     * @Delete(path = "/{categoryId}", name = "category_delete", requirements = {"categoryId"="\d+"})
     */
    public function deleteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('AppBundle:Category')->find($request->get('categoryId'));

        if (empty($category)) {
            return new JsonResponse(['message' => 'Category not found for delete'],Response::HTTP_NOT_FOUND);
        }

        $em->remove($category);
        $em->flush();

        return new JsonResponse(['message' => 'Category deleted successfully'], Response::HTTP_OK);
    }

}
