<?php
/**
 * Created by PhpStorm.
 * User: PC
 * Date: 17/10/2018
 * Time: 18:49
 */

namespace AppBundle\Model;

class RequestNewProduct
{
    private $name;
    private $description;
    private $brandId;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getBrandId()
    {
        return $this->brandId;
    }

    /**
     * @param mixed $brandId
     */
    public function setBrand($brandId)
    {
        $this->brandId = $brandId;
    }


}