<?php
/**
 * Created by PhpStorm.
 * User: PC
 * Date: 17/10/2018
 * Time: 12:58
 */

namespace AppBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use AppBundle\Entity\Brand;
use AppBundle\Entity\Product;
use AppBundle\Entity\Category;


class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $listBrands = array();
        // create 3 brands!
        for ($i = 0; $i < 5; $i++) {
            $brand = new Brand();
            $brand->setName('brand '.$i);
            $manager->persist($brand);
            $listBrands[] = $brand;

        }

        $listCategories = array();
        // create 5 categories!
        for ($i = 0; $i < 5; $i++) {
            $category = new Category();
            $category->setName('category '.$i);
            $manager->persist($category);
            $listCategories[] = $category;
        }

        // create 5 products!
        for ($i = 0; $i < 10; $i++) {
            $product = new Product();
            $product->setName('product '.$i);
            $product->setActive(($i%3)==0?true:false);
            $product->setBrand($listBrands[$i%3]);
            $product->setUrl('product/'.$i);
            $product->setDescription('description product '.$i);

            if(($i%3)==0){
                for ($j = 0; $j < ($i%5); $j++) {
                    $product->addCategory($listCategories[$j]);
                }
            }

            $manager->persist($product);
        }

        $manager->flush();
    }
}